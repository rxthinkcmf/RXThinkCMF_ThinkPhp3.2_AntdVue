<?php
// +----------------------------------------------------------------------
// | RXThinkCMF敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 RXThinkCMF并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: @牧羊人 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

return array(
    //'配置项'=>'配置值'
    /**
     * 性别
     */
    'GENDER_LIST' => array(
        1 => '男',
        2 => '女',
        3 => '未知',
    ),

    /**
     * 菜单类型
     */
    'MENU_TYPE' => array(
        1 => '模块',
        2 => '导航',
        3 => '菜单',
        4 => '节点',
    ),

    /**
     * 权限节点
     */
    'MENU_FUNC' => array(
        1 => '列表',
        5 => '添加',
        10 => '修改',
        15 => '删除',
        20 => '详情',
        25 => '状态',
        30 => '批量删除',
        35 => '添加子级',
        40 => '全部展开',
        45 => '全部折叠',
        50 => '导入数据',
        55 => '导出数据',
        60 => '设置权限',
        65 => '重置密码'
    ),

    /**
     * 部门类型
     */
    'DEPT_TYPE' => array(
        1 => '公司',
        2 => '子公司',
        3 => '部门',
        4 => '小组'
    ),

    /**
     * 配置类型
     */
    'CONFIG_TYPE' => array(
        'hidden' => '隐藏',
        'readonly' => '只读文本',
        'number' => '数字',
        'text' => '单行文本',
        'textarea' => '多行文本',
        'array' => '数组',
        'password' => '密码',
        'radio' => '单选框',
        'checkbox' => '复选框',
        'select' => '下拉框',
        'icon' => '字体图标',
        'date' => '日期',
        'datetime' => '时间',
        'image' => '单张图片',
        'images' => '多张图片',
        'file' => '单个文件',
        'files' => '多个文件',
        'ueditor' => '富文本编辑器',
        'json' => 'JSON',
    ),

    /**
     * 城市级别
     */
    'CITY_LEVEL' => array(
        1 => "省份",
        2 => "城市",
        3 => "区县",
        4 => "街道"
    ),

    /**
     * 友链类型
     */
    'LINK_TYPE' => array(
        1 => '友情链接',
        2 => '合作伙伴',
    ),

    /**
     * 友链形式
     */
    'LINK_FORM' => array(
        1 => '文字链接',
        2 => '图片链接',
    ),

    /**
     * 友链平台
     */
    'LINK_PLATFORM' => array(
        1 => 'PC站',
        2 => 'WAP站',
        3 => '小程序',
        4 => 'APP应用',
    ),

    /**
     * 广告投放平台
     */
    'AD_PLATFORM' => array(
        1 => 'PC站',
        2 => 'WAP站',
        3 => '小程序',
        4 => 'APP应用',
    ),

    /**
     * 广告类型
     */
    'AD_TYPE' => array(
        1 => '图片',
        2 => '文字',
        3 => '视频',
        4 => '其他',
    ),

    /**
     * 站点类型
     */
    'ITEM_TYPE' => array(
        1 => '普通站点',
    ),

    /**
     * 通知来源
     */
    'NOTICE_SOURCE' => array(
        1 => '系统平台',
    ),

    /**
     * 通知状态
     */
    'NOTICE_STATUS' => array(
        1 => '草稿箱',
        2 => '立即发布',
        3 => '定时发布',
    ),

    /**
     * 注册来源
     */
    'MEMBER_SOURCE' => array(
        1 => '注册会员',
        2 => '马甲会员',
    ),

    /**
     * 注册设备
     */
    'MEMBER_DEVICE' => array(
        0 => '未知',
        1 => '苹果',
        2 => '安卓',
        3 => 'WAP站',
        4 => 'PC站',
        5 => '微信小程序',
        6 => '后台添加',
    ),
);
